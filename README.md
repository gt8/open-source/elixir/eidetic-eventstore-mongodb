# Eidetic (EventSourcing for Elixir)

## EventStore MongoDB Adapter

[![pipeline status](https://gitlab.com/gt8/open-source/elixir/eidetic-eventstore-mongodb/badges/master/pipeline.svg)](https://gitlab.com/gt8/open-source/elixir/eidetic-eventstore-mongodb/commits/master)
[![coverage report](https://gitlab.com/gt8/open-source/elixir/eidetic-eventstore-mongodb/badges/master/coverage.svg)](https://gitlab.com/gt8/open-source/elixir/eidetic-eventstore-mongodb/commits/master)
