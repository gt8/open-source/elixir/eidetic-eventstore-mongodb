TESTS=

.PHONY: deps test

up:
	@mix run --no-halt

hex:
	@mix do local.hex --force, local.rebar --force

deps: hex
	@mix deps.get

compile:
	@mix compile

format:
	@mix format --check-equivalent

lint:
	@mix format --check-formatted
	@mix credo --strict

test:
	@MIX_ENV=test mix coveralls ${TESTS}

iex:
	@iex -S mix

clean:
	@mix clean --deps

#######
# Docker Stuff
#######
dshell:
	@docker-compose run --rm --entrypoint=bash elixir

dclean:
	@docker-compose run --rm --entrypoint make elixir clean
	@docker-compose down -v
