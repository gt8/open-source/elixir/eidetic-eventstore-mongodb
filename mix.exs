defmodule Eidetic.EventStore.MongoDB.Mixfile do
  use Mix.Project

  def project do
    [
      app: :eidetic_eventstore_mongodb,
      version: "1.0.0-alpha1",
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      elixirc_paths: elixirc_paths(Mix.env()),
      description: description(),
      package: package()
    ]
  end

  def application do
    [
      extra_applications: [
        :logger
      ]
    ]
  end

  def deps do
    [
      {:eidetic, ">= 0.0.0"},
      {:mongodb, "~> 0.4.4"},
      {:poolboy, ">= 1.5.1"},
      {:uuid, "~> 1.1"},
      {:credo, "~> 0.7", only: [:dev, :test]},
      {:excoveralls, "~> 0.7", only: :test},
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:faker, "~> 0.9", only: :test}
    ]
  end

  defp elixirc_paths(:dev), do: ["lib"]
  defp elixirc_paths(:test), do: ["test", "test/support"] ++ elixirc_paths(:dev)
  defp elixirc_paths(_), do: ["lib"]

  defp description do
    """
    A MongoDB EventStore for the Eidetic EventSourcing library
    """
  end

  defp package do
    [
      name: :eidetic_eventstore_mongodb,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["David McKay"],
      licenses: ["MIT"],
      links: %{
        "Source Code" => "https://gitlab.com/gt8/open-source/elixir/eidetic-eventstore-mongodb"
      }
    ]
  end
end
