defmodule TestSupport.EventFactory do
  def get(identifier \\ UUID.uuid4(), serial_number \\ 1) do
    # When using `:database` time, we lose the micro-seconds with mongoDB,
    # as we're using a timestamp, which we use for the ordinal sequence.
    # So lets wipe out the microseconds and assert
    %Eidetic.Event{
      type: Faker.Pokemon.name(),
      version: 1,
      identifier: identifier,
      serial_number: serial_number,
      datetime: Map.put(DateTime.utc_now(), :microsecond, {0, 3}),
      payload: %{
        color: Faker.Commerce.color(),
        name: Faker.Commerce.product_name(),
        price: Faker.Commerce.price()
      }
    }
  end
end
