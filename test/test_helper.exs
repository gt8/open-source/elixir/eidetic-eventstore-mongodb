require Logger
Logger.remove_backend(:console)
Logger.add_backend(:console, flush: true)

ExUnit.start()
Mongo.start_link([name: :mongo, seeds: ["mongo:27017"], database: "events", pool: DBConnection.Poolboy, w: :majority, j: true])
