defmodule Eidetic.EventStore.SystemTest do
  use ExUnit.Case, async: true

  setup do
    # Not passing a :date_type option should use :system
    {:ok, pid} = Eidetic.EventStore.MongoDB.start_link()

    {:ok, %{pid: pid}}
  end

  test "It returns an empty list of events if identifier is not found", %{pid: pid} do
    {:ok, []} = GenServer.call(pid, {:fetch, "unknown"})
  end

  test "It can insert an event into the event-store", %{pid: pid} do
    assert {:ok, [object_identifier: _]} = GenServer.call(pid, {:record, TestSupport.EventFactory.get()})
  end

  setup do
    # Not passing a :date_type option should use :system
    {:ok, pid} = Eidetic.EventStore.MongoDB.start_link()

    aggregate_one = UUID.uuid4()

    events = [
      TestSupport.EventFactory.get(aggregate_one, 1),
      TestSupport.EventFactory.get(aggregate_one, 2),
      TestSupport.EventFactory.get(aggregate_one, 3),
      TestSupport.EventFactory.get(),
      TestSupport.EventFactory.get()
    ]

    Enum.each(events, fn(event) ->
      GenServer.call(pid, {:record, event})
    end)

    on_exit fn() ->
      Enum.each(events, fn(event) ->
        Mongo.delete_one(:mongo, "events", %{identifier: event.identifier}, [pool: DBConnection.Poolboy])
      end)
    end

    {:ok, %{pid: pid, events: events}}
  end

  test "It can fetch an event from the event store", %{pid: pid, events: [event | _tail]} do
    {:ok, [database_event | _tail]} = GenServer.call(pid, {:fetch, event.identifier})

    assert event == database_event
  end

  test "It can fetch a limited set of events from the event store", %{pid: pid, events: [event_one | tail]} do
    {:ok, events} = GenServer.call(pid, {:fetch_until, event_one.identifier, 1})
    assert [event_one] == events

    {:ok, events} = GenServer.call(pid, {:fetch_until, event_one.identifier, 2})
    assert [event_one, List.first(tail)] == events
  end
end
