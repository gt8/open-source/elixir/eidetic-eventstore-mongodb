use Mix.Config

config :logger, :console,
  level: :debug,
  flush: true
